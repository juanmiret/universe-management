class SliderPhoto < ActiveRecord::Base
  belongs_to :slider
  has_attached_file :file, styles: { full: ['1280x960', :jpg] }
  validates_attachment_content_type :file, content_type: ['image/png', 'image/jpeg', 'image/jpg']
  dropzone_item data: :file,
                file_size: :file_file_size,
                title: :file_file_name,
                url: :data_url,
                container_id: :slider_id,
                position: :position
end
