class Slider < ActiveRecord::Base
  has_many :slider_photos
  accepts_nested_attributes_for :slider_photos, allow_destroy: true
end
