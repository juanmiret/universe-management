class Model < ActiveRecord::Base
  belongs_to :sex
  belongs_to :category



  scope :boys_mainboard, -> {
    sex = Sex.find_by_name('Boy')
    category = Category.find_by_name('Mainboard')
    where(sex: sex, category: category)
  }

  scope :boys_development, -> {
    sex = Sex.find_by_name('Boy')
    category = Category.find_by_name('Development')
    where(sex: sex, category: category)
  }

  scope :girls_mainboard, -> {
    sex = Sex.find_by_name('Girl')
    category = Category.find_by_name('Mainboard')
    where(sex: sex, category: category)
  }

  scope :girls_development, -> {
    sex = Sex.find_by_name('Girl')
    category = Category.find_by_name('Development')
    where(sex: sex, category: category)
  }

  scope :vision, -> {
    category = Category.find_by_name('Vision')
    where(category: category)
  }

  scope :paradise, -> {
    category = Category.find_by_name('Paradise')
    where(category: category)
  }

  scope :girls_paradise, -> {
    sex = Sex.find_by_name('Girl')
    category = Category.find_by_name('Paradise')
    where(sex: sex, category: category)
  }

  scope :boys_paradise, -> {
    sex = Sex.find_by_name('Boy')
    category = Category.find_by_name('Paradise')
    where(sex: sex, category: category)
  }

  has_many :photos, dependent: :destroy
  accepts_nested_attributes_for :photos, allow_destroy: true
  has_many :polaroids, dependent: :destroy
  accepts_nested_attributes_for :polaroids, allow_destroy: true

  has_attached_file :comp_card
  has_attached_file :pdf
  validates_attachment_content_type :comp_card, content_type: ['application/pdf']
  validates_attachment_content_type :pdf, content_type: ['application/pdf']

  def instagram_url
    "https://instagram.com/#{instagram}"
  end

end
