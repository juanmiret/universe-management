class Photo < ActiveRecord::Base
  belongs_to :model

  has_attached_file :file, styles: { full: ['1280x960', :jpg], index: ['386x565>', :jpg] }
  dropzone_item data: :file,
                file_size: :file_file_size,
                title: :file_file_name,
                url: :data_url,
                container_id: :model_id,
                position: :position


  validates_attachment_content_type :file, content_type: ['image/png', 'image/jpeg', 'image/jpg']

  def orientation
    Paperclip::Geometry.from_file(self.file).horizontal? ? 'horizontal' : 'vertical'
  end

end
