ActiveAdmin.register Slider do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name,
                slider_photo_ids: [], # for model Image
                slider_photos_attributes: [:id, :title, :position, :data, :data_file_size, :data_url, :slider_id, :_destroy]

  form do |f|
    f.semantic_errors
    inputs "Main" do
      input :name
      input :slider_photos, as: :dropzone
    end

    f.actions
  end

end
