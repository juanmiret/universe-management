ActiveAdmin.register Model do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :jeans, :overlay_color, :name, :sex_id, :category_id, :height, :chest, :waist, :hip, :suit, :shoe, :hair, :eyes, :instagram,
                :comp_card,
                :pdf,
                :video,
                photo_ids: [], # for model Image
                photos_attributes: [:id, :title, :position, :data, :data_file_size, :data_url, :model_id, :_destroy],
                polaroid_ids: [], # for model Image
                polaroids_attributes: [:id, :title, :position, :data, :data_file_size, :data_url, :model_id, :_destroy]


  scope :all, default: true
  scope :boys_mainboard
  scope :girls_mainboard
  scope :boys_development
  scope :girls_development
  scope :vision

  index do |m|
    selectable_column
    column "Profile" do |m|
      link_to edit_admin_model_path(m) do
        image_tag m.photos.find_by_position(0).file.url, class: 'ui tiny image' if m.photos.find_by_position(0)
      end
    end
    column :name do |m|
      link_to edit_admin_model_path(m) do
        m.name
      end
    end
    column :height
    column :chest
    column :waist
    column :hip
    column :suit
    column :jeans
    column :shoe
    column :hair
    column :eyes
    column :instagram
    actions defaults: false do |m|
      item "View", model_path(m)
      item "Delete", admin_model_path(m), method: :delete
    end
  end

  form do |f|
    f.semantic_errors
    inputs 'Basic Info', id: 'main-inputs' do
      input :sex
      input :category
      input :name
      input :instagram
    end
    inputs "Attributes", id: 'attributes-inputs' do
      input :height
      input :chest
      input :waist
      input :hip
      input :suit
      input :jeans
      input :shoe
      input :hair
      input :eyes
    end
    inputs "Book" do
      input :photos, as: :dropzone
    end
    inputs "Polaroids" do
      input :polaroids, as: :dropzone
    end
    inputs "Files" do
      input :video
      input :comp_card, as: :file, hint: object.comp_card_file_name
      input :pdf, as: :file, hint: object.pdf_file_name
    end
    f.actions
  end

  filter :sex
  filter :category
  filter :name
  filter :instagram

  controller do
    def update
      super do |success,failure|
        success.html { redirect_to collection_path }
      end
    end
  end

end
