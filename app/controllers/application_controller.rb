class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # before_filter :redirect_to_disabled_page

  private

  def redirect_to_disabled_page
    redirect_to(disabled_path) unless action_name == 'disabled'
  end
end
