class ModelsController < ApplicationController
  before_action :set_model_and_photos, only: [:show]

  def index
    @sex = Sex.find_by_name(params[:sex].capitalize)
    @category = Category.find_by_name(params[:category].capitalize)
    @models = Model.where(category: @category, sex: @sex)
  end

  def vision
    @models = Model.vision
    render template: 'models/index'
  end

  def paradise
    @models = Model.paradise
    render template: 'models/index'
  end

  def show
    render layout: 'fullscreen'
  end

  private
  def set_model_and_photos
    @model = Model.find(params[:id])
    case params[:photo_type]
      when 'polaroids'
        @photos = @model.polaroids
      else
        @photos = @model.photos
    end
  end

end
