class HomeController < ApplicationController
  def fullscreen
    render layout: 'fullscreen'
  end

  def home
    @slider = Slider.first
  end

  def contact
    @no_footer = true;
  end

  def disabled
    render layout: 'fullscreen'
  end

end
