module ApplicationHelper
  def instagram_url
    "https://instagram.com/universemanagement"
  end

  def facebook_url
    "https://facebook.com/UniverseManagement"
  end

  def blog_url
    "https://tumblr.com"
  end
end
