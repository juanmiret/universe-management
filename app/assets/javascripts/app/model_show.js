var renderArrows = function () {
    if ($('.slick-active').hasClass('first-slide')) {
        $('#prevSlide').hide();
    } else {
        $('#prevSlide').show();
    }

    if ($('.slick-active').hasClass('last-slide')) {
        $('#nextSlide').hide();
    } else {
        $('#nextSlide').show();
    }
    console.log('render arrows')
};

var setSliderWidth = function () {
    /*var width = 0;
    $('.slick-active').each(function () {
        width += $(this).width()
    });
    $('.slick-list').width(width);
    console.log('set slider width')*/
};

$(function () {

    $('.popup-youtube').magnificPopup({
        type: 'iframe'
    });

    $('.model-slider').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 2,
        speed: 0,
        variableWidth: true,
        swipe: false,
        touchMove: false,
        draggable: false,
        accessibility: false,
        lazyLoad: 'ondemand'

    }).on('afterChange', function(event, slick, currentSlide, nextSlide){
        setSliderWidth();
        renderArrows();
    }).on('lazyLoaded', function (event, slick, image, imageSource) {
        setSliderWidth();
    });
    setSliderWidth();
    renderArrows();



    $('#prevSlide').click(function () {
        $('.model-slider').slick('slickPrev')
    });
    $('#nextSlide').click(function () {
        $('.model-slider').slick('slickNext')
    });

});

$(document).keydown(function(e) {
    e.preventDefault();
    switch(e.which) {
        case 37: // left
            $('.model-slider').slick('slickPrev');
            break;

        case 38: // up
            break;

        case 39: // right
            $('.model-slider').slick('slickNext');
            break;

        case 40: // down
            break;

        default: return; // exit this handler for other keys
    }
     // prevent the default action (scroll / move caret)
});
