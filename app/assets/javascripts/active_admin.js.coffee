#= require active_admin/base
#= require activeadmin_addons/all
#= require activeadmin-dropzone
#= require semantic_ui/semantic_ui

$ ->
  $('#sidebar').detach().prependTo('#active_admin_content');