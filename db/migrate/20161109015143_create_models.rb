class CreateModels < ActiveRecord::Migration
  def change
    create_table :models do |t|
      t.string :name
      t.string :height
      t.string :chest
      t.string :waist
      t.string :shoe
      t.string :hair
      t.string :eyes
      t.string :instagram

      t.timestamps null: false
    end
  end
end
