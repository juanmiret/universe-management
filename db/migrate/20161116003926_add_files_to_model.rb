class AddFilesToModel < ActiveRecord::Migration
  def change
    add_attachment :models, :comp_card
    add_attachment :models, :pdf
    add_column :models, :video, :string
  end
end
