class CreateSliderPhotos < ActiveRecord::Migration
  def change
    create_table :slider_photos do |t|
      t.attachment :file
      t.timestamps null: false
    end
  end
end
