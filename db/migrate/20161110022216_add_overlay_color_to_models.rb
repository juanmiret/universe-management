class AddOverlayColorToModels < ActiveRecord::Migration
  def change
    add_column :models, :overlay_color, :string
  end
end
