class AddAttachmentToPolaroids < ActiveRecord::Migration
  def self.up
    change_table :polaroids do |t|
      t.attachment :file
    end
  end

  def self.down
    remove_attachment :polaroids, :file
  end
end
