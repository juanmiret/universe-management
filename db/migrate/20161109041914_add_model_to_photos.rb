class AddModelToPhotos < ActiveRecord::Migration
  def change
    add_reference :photos, :model, index: true, foreign_key: true
  end
end
