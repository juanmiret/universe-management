class AddDataToSliderPhotos < ActiveRecord::Migration
  def change
    add_reference :slider_photos, :slider, index: true, foreign_key: true
    add_column :slider_photos, :title, :string
    add_column :slider_photos, :position, :integer
  end
end
