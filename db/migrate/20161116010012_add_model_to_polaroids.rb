class AddModelToPolaroids < ActiveRecord::Migration
  def change
    add_reference :polaroids, :model, index: true, foreign_key: true
  end
end
