class AddSuitToModels < ActiveRecord::Migration
  def change
    add_column :models, :suit, :string
  end
end
