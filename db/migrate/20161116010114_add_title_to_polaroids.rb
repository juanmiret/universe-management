class AddTitleToPolaroids < ActiveRecord::Migration
  def change
    add_column :polaroids, :title, :string
    add_column :polaroids, :position, :integer
  end
end
