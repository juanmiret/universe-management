# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
AdminUser.create!(email: 'juanmiret55@icloud.com', password: 'v360656589', password_confirmation: 'v360656589')
Sex.create!(name: 'Boy')
Sex.create!(name: 'Girl')
Category.create!(name: 'Mainboard')
Category.create!(name: 'Development')
Category.create!(name: 'Vision')
